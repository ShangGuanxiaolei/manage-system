import fetch from 'reco-fetch'
/*
 *@method http请求
 *@param {string}mode 是否cors跨域
 *       {string}url api路径
 *       {string}method 请求方式（get/post)
 *       {object}headers 请求头
 *       {number}timeout 请求延长时间
 *       {object}body 请求体
 *       {object}params url参数
 *       {string}type 仅在post请求，可以设置：json / formData
 * 参考文献：https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#Supplying_request_options
*/
function httpRequest (url, { method, headers, timeout, body, params, type}) {
  const options = {
    mode: "cors",
    method: method || 'get',
    headers: headers || {'Content-Type': 'application/json'},
    timeout: timeout || 60000,
    body: JSON.stringify(body) || null,
    params: params || null,
    type: method === 'post' ? type : null
  }
  return fetch(url, options).then(res => {
    return res
  }).catch(err => {
    return {
      httpRequestError: `${err.message}，http status：${err.status}` || '网络异常，请稍后重试'
    }
  })
}

export { // 直接调用this.$http
  httpRequest
} 