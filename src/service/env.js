let env = 'uat' // prod or uat or dev or local or sit
let envLoacl = 'uat' // 当env为local值有效，有效值：uat or dev, 为了配合本地调试用，结合isOpenLocal字段
const domain = window.location.host
/**
 * @param {object}urlTotal 所有平台api域名管理
 *        {string}prod 生产环境
 *        {string}uat 测试环境
 *        {string}dev 开发环境
 *        {string}sit sit环境
 *        {string}stg stg环境
 *        {local}local 本地联调ip
 *        {boolean}isOpenLocal 是否开启本地调试
 * PS: 域名结尾请记得加'/'
 */
const urlTotal = {
  hdsUrlTotal: { // 中台
    prod: 'https://prologin.handeson.com/',
    uat: 'https://uatcenterapi.handeson.com/',
    uat2: 'https://uat2centerapi.handeson.com/',
    dev: 'https://devcenterapi.handeson.com/',
    sit: 'https://sitlogin.handeson.com/',
    stg: 'https://uatcenterapi.handeson.com/',
    local: 'http://192.168.43.189:8080/',
    isOpenLocal: true
  },
  sqUrlTotal: { // 社区
    prod: 'https://prosqapi.handeson.com/',
    uat: 'https://uatsqapi.handeson.com/',
    uat2: 'https://uat2sqapi.handeson.com/',
    dev: 'https://devsqapi.handeson.com/',
    sit: 'https://sitsqapi.handeson.com/',
    stg: 'https://uatsqapi.handeson.com/',
    local: 'http://192.168.43.218:8080/',
    isOpenLocal: false
  },
  hanweiUrlTotal: { // 汉薇
    prod: 'https://prohvm.handeson.com/',
    uat: 'https://uathvm.handeson.com/',
    uat2: 'https://uat2hvm.handeson.com/',
    dev: 'https://devhvm.handeson.com/',
    sit: 'https://sithvm.handeson.com/',
    stg: 'https://stghvm.handeson.com/',
    local: 'http://192.168.0.106:8080/',
    isOpenLocal: true
  },
  viviLifeUrlTotal: { // 薇莱芙
    prod: 'https://vivilife.handeson.com/',
    uat: 'https://vivilifetest.handeson.com/',
    uat2: 'https://vivilifetest.handeson.com/',
    dev: 'https://vivilifetest.handeson.com/',
    sit: 'https://vivilifetest.handeson.com/',
    stg: 'https://vivilifetest.handeson.com/',
    local: 'http://10.20.20.175:8080/',
    isOpenLocal: false
  },
  hwUrlTotal: { // 汉薇后台api
    dev: 'https://devhwsc.handeson.com/',
    uat: 'https://uathwsc.handeson.com/',
    uat2: 'https://uathwsc.handeson.com/',
    sit: 'https://sithwsc.handeson.com/',
    stg: 'https://stghwsc.handeson.com/',
    prod: 'https://hwsc.handeson.com/',
    local: 'http://hwsc-dev.handeson.com/',
    isOpenLocal: false
  },
  sprintUrlTotal: { // 基因
    dev: 'https://devhwsc.handeson.com/',
    uat: 'https://uathwapi.handeson.com/',
    uat2: 'https://uat2hwapi.handeson.com/',
    sit: 'https://sithwapi.handeson.com/',
    stg: 'https://stghwsc.handeson.com/',
    prod: 'https://hwapi.handeson.com/',
    local: 'http://10.20.20.234:80/',
    isOpenLocal: false
  }
}

if (domain.includes('hdscenter')) {
  env = 'prod'
  envLoacl = 'prod'
}
else {
  let index = domain.indexOf('center')
  let host = domain.substring(0, index)
  if (['dev', 'sit', 'uat', 'uat2', 'stg'].includes(host)) env = host
}

const url = {
  hdsUrl: urlTotal.hdsUrlTotal[
    env === 'local' 
    ? urlTotal.hdsUrlTotal.isOpenLocal ? 'local' : envLoacl
    : env
  ],
  sqUrl: urlTotal.sqUrlTotal[
    env === 'local' 
    ? urlTotal.sqUrlTotal.isOpenLocal ? 'local' : envLoacl
    : env
  ],
  hanweiUrl: urlTotal.hanweiUrlTotal[
    env === 'local' 
    ? urlTotal.hanweiUrlTotal.isOpenLocal ? 'local' : envLoacl
    : env
  ],
  viviLifeUrl: urlTotal.viviLifeUrlTotal[
    env === 'local'
    ? urlTotal.viviLifeUrlTotal.isOpenLocal ? 'local' : envLoacl
    : env
  ],
  hwUrl: urlTotal.hwUrlTotal[
    env === 'local'
    ? urlTotal.hwUrlTotal.isOpenLocal ? 'local' : envLoacl
    : env
  ],
  sprintUrl: urlTotal.sprintUrlTotal[
    env === 'local'
    ? urlTotal.sprintUrlTotal.isOpenLocal ? 'local' : envLoacl
    : env
  ]
}

export default url
