import url from '@/service/env'
const hdsUrl = url.hdsUrl

const getLargeArea = `${hdsUrl}forms/getRegion` // 获取大区
const getSmallArea = `${hdsUrl}forms/getSubRegionById` // 获取小区 or 所属店铺
const getIndustryType = `${hdsUrl}report/industry/list` // 获取行业信息
const downloadReport = `${hdsUrl}report/shops/list` // 导出报表
const specialLogin = `${hdsUrl}user/checkUser` // 通过token登陆中台

export {
  getLargeArea,
  getSmallArea,
  getIndustryType,
  downloadReport,
  specialLogin
}