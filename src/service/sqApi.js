import url from '@/service/env'
const sqUrl = url.sqUrl

const getNoPassResultUrl = `${sqUrl}publishCheckArticle/findReasonList` // 获取审核不通过的原因列表
const getUnauditedArticleNumUrl = `${sqUrl}check/unCheckHint` // 获取未审核文章的数量

export {
  getNoPassResultUrl,
  getUnauditedArticleNumUrl
}