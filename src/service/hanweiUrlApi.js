import url from '@/service/env'
const hanweiUrl = url.hanweiUrl // 汉薇api
/**
 * 商品促销API
 */
const queryActivityList = `${hanweiUrl}promotion/getActivityList` // 查询活动列表
const deleteOrStopActivity = `${hanweiUrl}promotion/deleteactivity` // 删除或终止活动
const seeOreditActivity = `${hanweiUrl}promotion/queryActivityProduct` // 查看或编辑活动
const getCommodityClassification = `${hanweiUrl}promotion/selectType` // 获取商品分类
const queryActivityProduct = `${hanweiUrl}promotion/selectActivityProduct` // 根据条件搜索商品
const saveActivity = `${hanweiUrl}promotion/creatPromotionBaseB` // 更新发布活动
const checkActiveInventory = `${hanweiUrl}promotion/checkStock` // 检查活动库存
const getActiveInventory = `${hanweiUrl}freshman/getStock` // 获取活动库存
const deleteGrandSale = `${hanweiUrl}grandSale/deleteGrandSale` // 大会促销删除活动
const deleteActivity = `${hanweiUrl}piece/group/deleteActivity` // 删除拼团活动
const deletedFlashSale = `${hanweiUrl}flashSale/deletedFlashSale` // 删除秒杀活动
const getGoodsMsg = `${hanweiUrl}promotion/getPromotionMessage` // 根据skucode获取商品信息
/**
 * 新客二期
 */
const checkActiveInventoryF = `${hanweiUrl}freshman/isSave` // 检查活动库存
/**
 * 代理请求
 */
const agentRequestApi = `${hanweiUrl}proxy/hvmall`
/**
 * 大会促销
 */
const insertGrandSaleShare = `${hanweiUrl}grandSale/insertGrandSaleShare` // 新增主会场文案
const updateGrandSaleMainShare = `${hanweiUrl}grandSale/updateGrandSaleMainShare` // 更新主会场文案
const getAllGrandSaleMainShare = `${hanweiUrl}grandSale/selectGrandSaleMainShare` // 获取所有主会场文案
const getLastMainShareTime = `${hanweiUrl}grandSale/selectLastMainShare` // 获取所有主会场文案中最晚结束时间
const getGrandSaleTimeline = `${hanweiUrl}grandSale/selectGrandSaleTimeline` // 获取时间轴列表数据
const deleteGrandSaleTimeline = `${hanweiUrl}grandSale/deleteGrandSaleTimeline` // 获取时间轴列表某一个节点数据
const insertGrandSaleTimeline = `${hanweiUrl}grandSale/insertGrandSaleTimeline` // 添加时间轴数据
/**
 * 秒杀
 */
const getSelectSystemSkuId = `${hanweiUrl}flashSale/selectSystemSkuId`
/**
 * 热门搜索
 */
const searchWord = `${hanweiUrl}product/listHotSearchKeys2` // 词条查询

export {
  queryActivityList,
  deleteOrStopActivity,
  seeOreditActivity,
  getCommodityClassification,
  queryActivityProduct,
  saveActivity,
  checkActiveInventory,
  checkActiveInventoryF,
  getActiveInventory,
  deleteGrandSale,
  deleteActivity,
  deletedFlashSale,
  agentRequestApi,
  insertGrandSaleShare,
  updateGrandSaleMainShare,
  getAllGrandSaleMainShare,
  getLastMainShareTime,
  getSelectSystemSkuId,
  getGoodsMsg,
  getGrandSaleTimeline,
  deleteGrandSaleTimeline,
  insertGrandSaleTimeline,
  searchWord
}