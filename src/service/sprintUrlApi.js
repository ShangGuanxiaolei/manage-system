import url from '@/service/env'
const sprintUrl = url.sprintUrl // 基因
/**
 * 外部订单导入导出
 */
const searchExportOrder = `${sprintUrl}v2/or/getOrderList` // 查询外部订单导入
const searchLog = `${sprintUrl}v2/or/getLogList` // 日志查询
const exportLog = `${sprintUrl}v2/or/getInfoList/exportExcel` // 导出erp日志
const exportLogNoErp = `${sprintUrl}v2/or/getInfoList/exportExcel/notErp` // 导出非erp日志
const seeLogDetail = `${sprintUrl}v2/or/getInfoDetails/Erp` // 查看erp日志详情
const seeNoErpLogDetail = `${sprintUrl}v2/or/getInfoDetails/notErp` // 查看erp日志详情
const seachExamine = `${sprintUrl}v2/or/getCheckList` // 查询审核
const orderExamine = `${sprintUrl}v2/or/submitCheck` // 审核订单
const importExcelErp = `${sprintUrl}v2/or/excelUpload/erp` // erp导入
const importExcelNoErp = `${sprintUrl}v2/or/excelUpload` // 非erp导入
const exportErpData = `${sprintUrl}v2/or/getErpList` //  erp导出
const exportNotErpData = `${sprintUrl}v2/or/getNotErpWl` //  非erp导出
const getLogtype = `${sprintUrl}v2/or/getInfoDetails/lot` // 区分日志是erp还是非erp

export {
  searchExportOrder,
  searchLog,
  exportLog,
  exportLogNoErp,
  seeLogDetail,
  seeNoErpLogDetail,
  seachExamine,
  orderExamine,
  importExcelErp,
  importExcelNoErp,
  exportErpData,
  exportNotErpData,
  getLogtype
}