import {
  ELTABLEDATAEDIT,
  TABLELODINGEDIT,
  TABLEDATASTOTALEDIT,
  ELCHECKBOXNUM,
  STRARR,
  FRESHMANPRODUCTS
} from '@/store/types'

const state = {
  elTableEditData: [],
  talbeLoadingEdit: false,
  tableDataTotal: 0,
  elCheckboxNum: 1,
  atrArr: [],
  sendFreshmanProducts: []
}

const getters = {
  elTableEditData: state => state.elTableEditData,
  talbeLoadingEdit: state => state.talbeLoadingEdit,
  tableDataTotal: state => state.tableDataTotal,
  elCheckboxNum: state => state.elCheckboxNum,
  atrArr: state => state.atrArr,
  sendFreshmanProducts: state => state.sendFreshmanProducts
}

const mutations = {
  [ELTABLEDATAEDIT] (state, payload) {
    state.elTableEditData = payload
  },
  [TABLELODINGEDIT] (state, payload) {
    state.talbeLoadingEdit = payload
  },
  [TABLEDATASTOTALEDIT] (state, payload) {
    state.tableDataTotal = payload
  },
  [ELCHECKBOXNUM] (state, payload) {
    switch(payload.type) {
      case 'add' : state.elCheckboxNum++
      break
      case 'reduce' : state.elCheckboxNum > 1 ? state.elCheckboxNum-- : state.elCheckboxNum = 1
      break
      case 'is' : state.elCheckboxNum - payload.value > 1 ? state.elCheckboxNum = state.elCheckboxNum - payload.value : state.elCheckboxNum = 1
      break
    }
  },
  [STRARR] (state, payload) {
    switch(payload.type) {
      case 'del' : 
      state.atrArr.map((item, index) => {
        if (item === payload.value) {
          state.atrArr.splice(index, 1)
          state.elCheckboxNum--
        }
      })
      break
      case 'push' : state.atrArr.push(payload.value)
      break
      case 'is' : state.atrArr = payload.value
      break
    }
  },
  [FRESHMANPRODUCTS] (state, payload) {
    switch (payload.type) {
      case 'clear' :
      state.sendFreshmanProducts = state.freshmanProducts = []
      break
      case 'sendFreshmanProducts' :
      state.sendFreshmanProducts = payload.value
      break
    }
  }
}

const actions = {
  saveElTableEditData ({ commit }, payload) {
    commit(ELTABLEDATAEDIT, payload)
  },
  saveTalbeLodingEditStatus ({commit}, payload) {
    commit(TABLELODINGEDIT, payload)
  },
  saveTableDatasTotal ({commit}, payload) {
    commit(TABLEDATASTOTALEDIT, payload)
  },
  changeElCheckboxNum ({commit}, payload) {
    commit(ELCHECKBOXNUM, payload)
  },
  changeStrArr ({commit}, payload) {
    commit(STRARR, payload)
  },
  saveFreshmanProducts ({commit}, payload) {
    commit(FRESHMANPRODUCTS, payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
