import {
  TABLEDATAP,
  ELTABLECOLUMN,
  TABLEBTNSP,
  TABELCONFIGP,
  PAGINATIONCONFIGP,
  BTNLOADDING,
  TABLECURRENTPAGE,
  TABLELODINGP,
  TABLESELECTBTNP
} from '@/store/types'

const state = {
  tableDatap: [],
  elTableColumn: [],
  tableBtnsP: [],
  tableConfigP: {},
  paginationConfigP: {
    show: true,
    PageSizesArr: [10, 50, 100],
    layout: 'total, sizes, prev, pager, next, jumper',
    total: 0,
    pageSize: 10,
    marginTop: '10px',
    justifyContent: 'flex-end'
  },
  btnLoading: {
    search: false,
    import: false,
    export: false,
    query: false
  },
  tableCurrentPage: 1,
  tableLoadingP: false,
  tableSelectBtnP: {
    type: 'primary',
    text: '全选',
    show: false,
    disabled: false
  }
}

const getters = {
  tableDatap: state => state.tableDatap,
  elTableColumn: state => state.elTableColumn,
  tableBtnsP: state => state.tableBtnsP,
  tableConfigP: state => state.tableConfigP,
  paginationConfigP: state => state.paginationConfigP,
  btnLoading: state => state.btnLoading,
  tableCurrentPage: state => state.tableCurrentPage,
  tableLoadingP: state => state.tableLoadingP,
  tableSelectBtnP: state => state.tableSelectBtnP
}

const mutations = {
  [TABLEDATAP] (state, payload) {
    state.tableDatap = payload
  },
  [ELTABLECOLUMN] (state, payload) {
    state.elTableColumn = payload
  },
  [TABLEBTNSP] (state, payload) {
    state.tableBtnsP = payload
  },
  [TABELCONFIGP] (state, payload) {
    state.tableConfigP = payload
  },
  [PAGINATIONCONFIGP] (state, payload) {
    state.paginationConfigP = Object.assign(state.paginationConfigP, payload)
  },
  [BTNLOADDING] (state, payload) {
    state.btnLoading = Object.assign(state.btnLoading, payload)
  },
  [TABLECURRENTPAGE] (state, payload) {
    state.tableCurrentPage = payload
  },
  [TABLELODINGP] (state, payload) {
    state.tableLoadingP = payload
  },
  [TABLESELECTBTNP] (state, payload) {
    state.tableSelectBtnP = Object.assign(state.tableSelectBtnP, payload)
  }
}

const actions = {
  saveTableDatap ({ commit }, payload) {
    commit(TABLEDATAP, payload)
  },
  saveElTableColumn ({commit}, payload) {
    commit(ELTABLECOLUMN, payload)
  },
  saveTableBtnsP ({commit}, payload) {
    commit(TABLEBTNSP, payload)
  },
  saveTableConfigP ({commit}, payload) {
    commit(TABELCONFIGP, payload)
  },
  savePaginationConfigP ({commit}, payload) {
    commit(PAGINATIONCONFIGP, payload)
  },
  saveBtnLoading ({commit}, payload) {
    commit(BTNLOADDING, payload)
  },
  saveTableCurrentPage ({commit}, payload) {
    commit(TABLECURRENTPAGE, payload)
  },
  saveTableLoadingP ({commit}, payload) {
    commit(TABLELODINGP, payload)
  },
  saveTableSelectBtnP ({commit}, payload) {
    commit(TABLESELECTBTNP, payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
