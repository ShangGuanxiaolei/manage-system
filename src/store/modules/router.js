import {
  ROUTERADDPATH,
  ACTIVELIST,
  ISTABLEFULL,
  TABLEHEIGHT
} from '@/store/types'

const state = {
  routerArr: [],
  activeList: '',
  isTableFull: true,
  tableHeight: 500
}

const getters = {
  routerArr: state => state.routerArr,
  activeList: state => state.activeList,
  isTableFull: state => state.isTableFull,
  tableHeight: state => state.tableHeight
}

const mutations = {
  [ROUTERADDPATH] (state, payload) {
    state.routerArr = payload
  },
  [ACTIVELIST] (state, payload) {
    state.activeList = payload
  },
  [ISTABLEFULL] (state, payload) {
    state.isTableFull = payload
  },
  [TABLEHEIGHT] (state, payload) {
    state.tableHeight = payload
  }
}

const actions = {
  addRouterPath ({ commit }, payload) {
    commit(ROUTERADDPATH, payload)
  },
  saveActiveList ({commit}, payload) {
    commit(ACTIVELIST, payload)
  },
  changeTableFull ({commit}, payload) {
    commit(ISTABLEFULL, payload)
  },
  changeTableHeight ({commit}, payload) {
    commit(TABLEHEIGHT, payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
