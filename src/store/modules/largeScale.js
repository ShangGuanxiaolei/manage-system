import {
  LARGESCALETABLEDATA
} from '@/store/types'

const state = {
  largeScaleTableData: {
    tableData: [],
    total: 0,
    loading: false
  }
}

const getters = {
  largeScaleTableData: state => state.largeScaleTableData
}

const mutations = {
  [LARGESCALETABLEDATA] (state, payload) {
    state.largeScaleTableData = Object.assign(state.largeScaleTableData, payload)
  }
}

const actions = {
  saveLargeScaleTableData ({ commit }, payload) {
    commit(LARGESCALETABLEDATA, payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
