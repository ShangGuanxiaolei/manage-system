import {
  BANNER_TABLEDATA,
  PAGETOTAL_TABLEDATA,
  BANNER_TABLELOADING,
  CLASSIFYOPTIONSBANNER,
  SEARCHINPUTBANNER,
  ISDEFAULTORSEARCH,
  SELECTROWDATA
} from '@/store/types'

const state = {
  bannerTableData: [],
  pageTotalBanner: 0,
  bannerLoading: false,
  classifyOptionsBanner: [{
    loading: false,
    disabled: false,
    options: [],
    value: '',
    id: 3
  },{
    loading: false,
    disabled: false,
    options: [],
    value: '',
    id: 4
  }],
  searchInputBanner: [{
    value: '',
    placeholder: '请输入商品名称/SKU编码'
  },{
    value: '',
    placeholder: '请输入商品ID'
  }],
  isDefaultOrSeach: true,
  selectRowData: {
    name: '',
    id: ''
  }
}

const getters = {
  bannerTableData: state => state.bannerTableData,
  pageTotalBanner: state => state.pageTotalBanner,
  bannerLoading: state => state.bannerLoading,
  classifyOptionsBanner: state => state.classifyOptionsBanner,
  searchInputBanner: state => state.searchInputBanner,
  isDefaultOrSeach: state => state.isDefaultOrSeach,
  selectRowData: state => state.selectRowData
}

const mutations = {
  [BANNER_TABLEDATA] (state, payload) {
    state.bannerTableData = payload
  },
  [PAGETOTAL_TABLEDATA] (state, payload) {
    state.pageTotalBanner = payload
  },
  [BANNER_TABLELOADING] (state, payload) {
    state.bannerLoading = payload
  },
  [CLASSIFYOPTIONSBANNER] (state, payload) {
    state.classifyOptionsBanner = payload
  },
  [SEARCHINPUTBANNER] (state, payload) {
    state.searchInputBanner = payload
  },
  [ISDEFAULTORSEARCH] (state, payload) {
    state.isDefaultOrSeach = payload
  },
  [SELECTROWDATA] (state, payload) {
    state.selectRowData = payload
  }
}

const actions = {
  saveBannerTableData ({ commit }, payload) {
    commit(BANNER_TABLEDATA, payload)
  },
  saveBannerPageTotal ({ commit }, payload) {
    commit(PAGETOTAL_TABLEDATA, payload)
  },
  saveBannerTableLoading ({ commit }, payload) {
    commit(BANNER_TABLELOADING, payload)
  },
  saveClassifyOptionsBanner ({commit}, payload) {
    commit(CLASSIFYOPTIONSBANNER, payload)
  },
  saveSearchInputBanner ({commit}, payload) {
    commit(SEARCHINPUTBANNER, payload)
  },
  saveIsDefaultSearch ({commit}, payload) {
    commit(ISDEFAULTORSEARCH, payload)
  },
  saveSelectRowData ({commit}, payload) {
    commit(SELECTROWDATA, payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
