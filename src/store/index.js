import Vue from 'vue'
import Vuex from 'vuex'
import commodityPromotion from '@/store/modules/commodityPromotion'
import routerJs from '@/store/modules/router'
import bannerManagement from '@/store/modules/bannerManagement'
import externalOrder from '@/store/modules/externalOrder'
import largeScale from '@/store/modules/largeScale'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    commodityPromotion, // 促销活动
    bannerManagement, // banner
    routerJs,
    externalOrder,
    bannerManagement, // banner
    largeScale // 大会促销
  }
})
