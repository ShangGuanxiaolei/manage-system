const Login = () => import(/* webpackChunkName: "group-login" */ './views/Login.vue')
const NotFound = () => import(/* webpackChunkName: "group-error" */ './views/404.vue')
const Home = () => import(/* webpackChunkName: "group-home" */ './views/Home.vue')
const Main = () => import(/* webpackChunkName: "group-home" */ './views/Main.vue')
const Table = () => import(/* webpackChunkName: "group-home" */ './views/nav1/Table.vue')
const Form = () => import(/* webpackChunkName: "group-home" */ './views/nav1/Form.vue')
const user = () => import(/* webpackChunkName: "group-home" */ './views/nav1/user.vue')
const UserSku = () => import(/* webpackChunkName: "group-home" */ './views/nav1/UserSku.vue')
const Page4 = () => import(/* webpackChunkName: "group-report" */ './views/report/Page4.vue')
const Query = () => import(/* webpackChunkName: "group-report" */ './views/report/Query.vue')
const QueryV2 = () => import(/* webpackChunkName: "group-report" */ './views/report/QueryV2.vue')
const OrderData = () => import(/* webpackChunkName: "group-report" */ './views/report/OrderData.vue')
const Report = () => import(/* webpackChunkName: "group-report" */ './views/report/Report.vue')
const Integration = () => import(/* webpackChunkName: "group-report" */ './views/report/Integration.vue')
const Achievement = () => import(/* webpackChunkName: "group-report" */ './views/report/Achievement.vue')
const Defend = () => import(/* webpackChunkName: "group-report" */ './views/report/Defend.vue')
const Recommend = () => import(/* webpackChunkName: "group-report" */ './views/report/Recommend.vue')
const Tevreport = () => import(/* webpackChunkName: "group-report" */ './views/report/Tevreport.vue')
const Simulator = () => import(/* webpackChunkName: "group-report" */ './views/report/Simulator.vue')
const LeaderQuery = () => import(/* webpackChunkName: "group-report" */ './views/report/LeaderQuery.vue')
const ProducSchedule = () => import(/* webpackChunkName: "group-report" */ './views/report/ProducSchedule.vue')
const ProductDetail = () => import(/* webpackChunkName: "group-report" */ './views/report/ProductDetail.vue')
const ProductDetailSKU = () => import(/* webpackChunkName: "group-report" */ './views/report/ProductDetailSKU.vue')
const WeeklyData = () => import(/* webpackChunkName: "group-report" */ './views/report/WeeklyData.vue')
// 2019.03 lqn begin
const MonthlyData = () => import(/* webpackChunkName: "group-report" */ './views/report/MonthlyData.vue')
const HVCheckData = () => import(/* webpackChunkName: "group-report" */ './views/report/HVCheckData.vue')
const EarningAmt = () => import(/* webpackChunkName: "group-report" */ './views/report/others/EarningAmt.vue')
const EarningPoint = () => import(/* webpackChunkName: "group-report" */ './views/report/others/EarningPoint.vue')
const ChuangshiTicket = () => import(/* webpackChunkName: "group-report" */ './views/report/others/ChuangshiTicket.vue')
const OlderBelonger = () => import(/* webpackChunkName: "group-report" */ './views/report/others/OlderBelonger.vue')
const SO_OWPromotion = () => import(/* webpackChunkName: "group-report" */ './views/report/others/SO_OWPromotion.vue')
const PointLines = () => import(/* webpackChunkName: "group-report" */ './views/report/others/PointLines.vue')
const AllFinance = () => import(/* webpackChunkName: "group-report" */ './views/report/finance/index.vue')
const AllTaxation = () => import(/* webpackChunkName: "group-report" */ './views/report/taxation/index.vue')
const AllSalesOrder = () => import(/* webpackChunkName: "group-report" */ './views/report/salesorder/index.vue')
const AllLogistics = () => import(/* webpackChunkName: "group-report" */ './views/report/logistics/index.vue')
const PreferredMerchant = () => import(/* webpackChunkName: "group-report" */ './views/report/preferredMerchant')
// 2019.03 lqn end
const PersonalManage = () => import(/* webpackChunkName: "group-activityManagement" */ './views/personal/Manage.vue')
const Page6 = () => import(/* webpackChunkName: "group-activityManagement" */ './views/nav3/Page6.vue')
const FigthList = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/FigthList.vue')
const ProductList = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/ProductList.vue')
const RecommendList = () => import (/* webpackChunkName: "group-activityManagement" */ './views/hvm/recommendList.vue') 
const EditActivity = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/EditActivity.vue')
const Statistic = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/Statistic.vue')
const Meeting = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/Meeting.vue')
const Bargain = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/Bargain.vue')
const ConferenceData = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/ConferenceData.vue')
const EditMeeting = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/EditMeeting.vue')
const Vip = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/VIP.vue')
const SecKill = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/SecKill.vue')
// const Page1 = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/Page1.vue')
const Activity = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/Activity.vue')
const ReserveList = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/ReserveList.vue')
const AddReserve = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/AddReserve.vue')
const NewChannel = () => import(/* webpackChunkName: "group-activityManagement" */ './views/hvm/NewChannel.vue')
const echarts = () => import(/* webpackChunkName: "group-activityManagement" */ './views/charts/echarts.vue')
const Service = () => import(/* webpackChunkName: "group-activityManagement" */ './views/community/Service.vue')
const AddContent = () => import(/* webpackChunkName: "group-activityManagement" */ './views/community/AddContent.vue')
const Category = () => import(/* webpackChunkName: "group-activityManagement" */ './views/community/Category.vue')
const Comment = () => import(/* webpackChunkName: "group-activityManagement" */ './views/community/Comment.vue')
const Content = () => import(/* webpackChunkName: "group-activityManagement" */ './views/community/Content.vue')
const DecentReward = () => import(/* webpackChunkName: "group-activityManagement" */ './views/community/DecentReward.vue')
const SensitiveWords = () => import(/* webpackChunkName: "group-activityManagement" */ './views/community/SensitiveWords.vue')
const Member = () => import(/* webpackChunkName: "group-activityManagement" */ './views/community/Member.vue')
const RcList = () => import(/* webpackChunkName: "group-activityManagement" */ './views/community/RcList.vue')
const CommentDetail = () => import(/* webpackChunkName: "group-activityManagement" */ './views/community/CommentDetail.vue')
const Carousel = () => import(/* webpackChunkName: "group-activityManagement" */ './views/MicroBusiness/Carousel.vue')
const Placard = () => import(/* webpackChunkName: "group-activityManagement" */ './views/MicroBusiness/Placard.vue')
const Publish = () => import(/* webpackChunkName: "group-activityManagement" */ './views/MicroBusiness/Publish.vue')
const Issue = () => import(/* webpackChunkName: "group-activityManagement" */ './views/onlineService/Issue.vue')
const Classification = () => import(/* webpackChunkName: "group-activityManagement" */ './views/onlineService/Classification.vue')
const ActivityList = () => import(/* webpackChunkName: "group-activityManagement" */ './views/bonus/activityList.vue')
const WinnerList = () => import(/* webpackChunkName: "group-activityManagement" */ './views/bonus/winnerList.vue')
/**
 * 商品促销
 */
const CommodityPromotion = () => import(/* webpackChunkName: "group-activityManagement" */ '@/views/activityManagement/commodityPromotion')
const CommodityPromotionHandle = () => import(/* webpackChunkName: "group-activityManagement" */ '@/views/activityManagement/commodityPromotion/Handle')
const PublishNewActivities = () => import(/* webpackChunkName: "group-activityManagement" */ '@/views/activityManagement/commodityPromotion/PublishNewActivities')
const LargeScale = () => import(/* webpackChunkName: "group-home" */ './views/hvm/LargeScale.vue')
const TimeBase = () => import(/* webpackChunkName: "group-home" */ './views/hvm/TimeBase.vue')
const MasterKV = () => import(/* webpackChunkName: "group-home" */ './views/hvm/MasterKV.vue')
/**
 * 外部订单
 */
const ExternalOrder = () => import(/* webpackChunkName: "group-externalOrder" */ '@/views/externalOrder')
const ExternalOrderReview = () => import(/* webpackChunkName: "group-externalOrder" */ '@/views/externalOrder/review.vue')
const Journal = () => import(/* webpackChunkName: "group-externalOrder" */ '@/views/externalOrder/journal')
const LogDetails = () => import(/* webpackChunkName: "group-externalOrder" */ '@/views/externalOrder/logDetails')
/*
 * 汉薇banner管理
 */
const HWbanner = () => import(/* webpackChunkName: "group-activityManagement" */ '@/views/activityManagement/HWbanner')
const HWbannerAdd = () => import(/* webpackChunkName: "group-activityManagement" */ '@/views/activityManagement/HWbanner/add')

const HotSearch = () => import(/* webpackChunkName: "group-activityManagement" */ '@/views/hotSearch') // 热门搜索
const VIPGoods = () => import(/* webpackChunkName: "group-activityManagement" */ '@/views/vipGoods') // vip资格商品

let routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },

    // { path: '/main', component: Main },
    {
        path: '/',
        component: Home,
        name: '系统管理',
        iconCls: 'el-icon-message',//图标样式class
        children: [
            { path: '/main', component: Main, name: '主页', hidden: true },
            // { path: '/table', component: Table, name: '用户管理' },
            // { path: '/form', component: Form, name: '角色管理' },
            // { path: '/user', component: user, name: '权限管理' },
            { path: '/user/userList', component: Table, name: '用户管理' },
            { path: '/auth/roleManage', component: Form, name: '角色管理' },
            { path: '/auth/permList', component: user, name: '权限管理' },
            { path: '/auth/skuPermList', component: UserSku, name: '用户SKU权限管理' },

        ]
    },
    {
        path: '/',
        component: Home,
        name: '人员管理',
        iconCls: 'fa fa-id-card-o',
        children: [
            { path: '/page4', component: Page4, name: '页面4' },
            { path: '/personal/manage', component: PersonalManage, name: '清除&退出' },
        ]
    },
    {
        path: '/',
        component: Home,
        name: '',
        iconCls: 'fa fa-address-card',
        leaf: true,//只有一个节点
        children: [
            { path: '/page6', component: Page6, name: '订单管理' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '活动管理',
        iconCls: 'el-icon-menu',
        // leaf: true,//只有一个节点
        children: [
            { path: '/fightList', component: FigthList, name: '拼团清单' },
            { path: '/recommendList', component: RecommendList, name: '商品推荐' },
            // { path : '/page1', component : Page1, name : '订单管理' },
            { path: '/activity', component: Activity, name: '活动监控' },
            { path: '/productList', component: ProductList, name: '商品列表', hidden: true },
            { path: '/meeting', component: Meeting, name: '会议促销' },
            { path: '/bargain', component: Bargain, name: '砍价管理' },
            { path: '/editActivity', component: EditActivity, name: '编辑拼团', hidden: true },
            { path: '/statistic', component: Statistic,name:'拼团统计',  hidden: true },
            { path: '/conferenceData', component: ConferenceData,name:'大会统计',  hidden: true },
            { path: '/meeting/editMeeting', component: EditMeeting,name:'编辑大会',  hidden: true },
            { path: '/newChannel', component: NewChannel,name:'新人频道' },
            { path: '/activityManagement/commodityPromotion', component: CommodityPromotion,name:'商品促销'},
            { path: '/activityManagement/commodityPromotion/handle', component: CommodityPromotionHandle,name:'商品促销操作' ,hidden: true },
            { path: '/activityManagement/commodityPromotion/publishNewActivities', component: PublishNewActivities,name:'发布新活动' ,hidden: true },
            { path: '/vip', component: Vip,name:'vip资格设置',  hidden: true },
            { path: '/reserveList', component: ReserveList,name:'预约预售' },
            { path: '/addReserve', component: AddReserve,name:'添加预约' },
            { path: '/secKill', component: SecKill,name:'秒杀',  hidden: true },
            { path: '/hotSearch', component: HotSearch,name:'热门搜索' },
            { path: '/vipGoods', component: VIPGoods,name:'VIP资格商品' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '大型促销',
        iconCls: 'el-icon-menu',
        // leaf: true,//只有一个节点
        children: [
            { path: '/timeBase', component: TimeBase, name: '时间轴' },
            { path: '/largeScale', component: LargeScale, name: '促销'},
            { path: '/masterKV', component: MasterKV, name: '主KV'},
            { path: '/hwBanner', component: HWbanner,name:'汉薇轮播图',  hidden: true },
            { path: '/hwBannerAdd', component: HWbannerAdd,name:'新增轮播图',  hidden: true }
        ]
    },
    {
        path:'/',
        component: Home,
        name: '红包雨&抽奖',
        iconCls:'el-icon-menu',
        children:[
            { path: '/activityList', component: ActivityList, name: '活动清单'},
            { path: '/winnerList', component: WinnerList, name: '中奖清单'},
        ]
    },
    {
        path: '/',
        component: Home,
        name: '报表管理',
        iconCls: 'fa fa-bar-chart',
        children: [
            { path: '/preferredMerchant', component: PreferredMerchant, name: '优选商家报表' },
            { path: '/query', component: Query, name: '加入人员表' },
            { path: '/query_v2', component: QueryV2, name: '加入人员表v2' },
            { path: '/orderData', component: OrderData, name: '订单数据表' },
            { path: '/report', component: Report, name: '中台晋升表' },
            { path: '/integration', component: Integration, name: '积分及加入量表' },
            { path: '/achievement', component: Achievement, name: '业绩刷新报表' },
            { path: '/defend', component: Defend, name: '业绩刷新目标维护' },
            { path: '/recommend', component: Recommend, name: '加入人员表-推荐人员' },
            { path: '/tevreport', component: Tevreport, name: 'TEV查询报表' },
            { path: '/simulator', component: Simulator, name: '五代业绩模拟器' },
            { path: '/leaderQuery', component: LeaderQuery, name: '领导人市场查询表' },
            { path: '/productSchedule', component: ProducSchedule, name: '产品明细表' },
            { path: '/weeklyData', component: WeeklyData, name: '销售数据统计表' },
            { path: '/productDetail', component: ProductDetail, name: '产品明细-城市' ,},
            { path: '/productDetailSKU', component: ProductDetailSKU, name: '产品明细-城市(SKU)' ,},
            { path: '/monthlyData', component: MonthlyData, name: '月结报表' ,},
            { path: '/HVCheckData', component: HVCheckData, name: 'HV对账报表' ,},
            { path: '/EarningAmt', component: EarningAmt, name: '秒结积分' ,},
            { path: '/EarningPoint', component: EarningPoint, name: ' 秒结德分' ,},
            { path: '/ChuangshiTicket', component: ChuangshiTicket, name: '创世&明星券' ,},
            { path: '/OlderBelonger', component: OlderBelonger, name: '订单归属人报表' ,},
            { path: '/SO_OWPromotion', component: SO_OWPromotion, name: 'SO_OW晋升报表' ,},
            { path: '/PointLines', component: PointLines, name: '德分额度报表' ,},
            { path: '/Finance', component: AllFinance, name: '财务报表' ,},
            { path: '/Taxation', component: AllTaxation, name: '税务报表' ,},
            { path: '/SalesOrder', component: AllSalesOrder, name: '销售报表' ,},
            { path: '/Logistics', component: AllLogistics, name: '物流报表' ,},
        ]
    },
    {
        path: '/',
        component: Home,
        name: '社区管理',
        iconCls: 'fa fa-bar-chart',
        children: [
            { path: '/community/service', component: Service, name: '审核文章' },
            { path: '/community/addContent', component: AddContent, name: '新建官方内容' },
            { path: '/community/category', component: Category, name: '类目管理' },
            { path: '/community/comment', component: Comment, name: '评论管理' ,meta: {
                keepAlive: true  // true 表示需要使用缓存  false表示不需要被缓存
              }},
            { path: '/community/commentDetail', component: CommentDetail, name: '评论详情' },
            { path: '/community/content', component: Content, name: '官方内容' },
            { path: '/community/decentReward', component: DecentReward, name: '德分奖励管理' },
            { path: '/community/sensitiveWords', component: SensitiveWords, name: '敏感词管理' },
            { path: '/community/member', component: Member, name: '会员管理' },
            { path: '/community/rcList', component: RcList, name: '白名单管理' },
        ]
    },
    {
        path: '/',
        component: Home,
        name: '微商管理',
        iconCls: 'fa fa-bar-chart',
        children: [
            { path: '/carousel', component: Carousel, name: '轮播图管理' },
            { path: '/placard', component: Placard, name: '公告管理'},
            { path: '/publish', component: Publish, name: '发布管理',},
            { path: '/placard', component: Placard, name: '提示管理'},
        ]
    },
    {
        path: '/',
        component: Home,
        name: '外部订单管理',
        iconCls: 'fa fa-bar-chart',
        children: [
            { path: '/externalOrder/import', component: ExternalOrder, name: '外部订单导入' },
            { path: '/externalOrder/review', component: ExternalOrderReview, name: '导入订单审核' },
            {path: '/externalOrder/journal', component: Journal, name: '订单日志' },
            {path: '/externalOrder/logDetails', component: LogDetails, name: '日志详情'}
        ]
    },
    {
        path: '/',
        component: Home,
        name: '常见问题管理',
        iconCls: 'fa fa-bar-chart',
        children: [
            { path: '/onlineService/issue', component: Issue, name: '问题管理' },
            { path: '/onlineService/classification', component: Classification, name: '分类管理' },
        ]
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    {
        path: '*',
        hidden: true,
        name: '',
        redirect: { path: '/' }
    },

];

export default routes;