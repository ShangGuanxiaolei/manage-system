import Vue from 'vue'
import moment from 'moment'

Vue.filter('dateformat', function(dataStr, format) {
  return moment(dataStr).format(format || 'YYYY-MM-DD HH:mm:ss')
})