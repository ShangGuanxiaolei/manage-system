import moment from 'moment'
import localforage from 'localforage'
import {agentRequestApi} from '@/service/hanweiUrlApi'
import env from '@/service/env'
import XLSX from "xlsx";
/**
 * @method dateChangeover
 * @description 时间与时间戳互转，入参类型是Array
 * @param {String | Number} data 接受值：时间，时间戳
 * @param {String} format 时间格式
 * @param {String} type 接受值：dateStamp（转换时间戳），dateformat（时间格式化）
 * @return type传入值是dateStamp，返回的是时间戳，type传入值是dateformat，返回的是时间
 */
function dateChangeover([data, format, type]) {
  if (!data) return ''
  switch (type) {
    case 'dateStamp':
      return moment(data, format || 'YYYY-MM-DD HH:mm:ss').valueOf();
    case 'dateformat':
      return moment(data).format(format || 'YYYY-MM-DD HH:mm:ss')
  }
}
/**
 * @method transferredMeaning
 * @description 转义，入参类型是Array，在data中，查询targetAttributeName的属性值等于value的对象，返回这个对象的correspondingAttributeName属性值
 * @param {Object} data
 * @param {String} targetAttributeName 目标属性名
 * @param {String} correspondingAttributeName 需要查找对应属性名
 * @param {String | Number} value 匹配值
 * @param {String} returnType 返回值类型，接受值：string/number
 * @return correspondingAttributeName属性值
 */
function transferredMeaning({ data, targetAttributeName, correspondingAttributeName, value, returnType }) {
  for (let [_, item] of data.entries()) {
    if (item[targetAttributeName] === value) {
      switch (returnType) {
        case 'string': return item[correspondingAttributeName] + ''
        case 'number': return item[correspondingAttributeName] - 0
      }
    }
  }
}
/**
 * @method localforageDo
 * @description 本地数据存储
 * @key string 键值
 * @value string 存储值
 * @type string 存或取
 * */
function localforageDo({ key, value, type }) {
  switch (type.toLowerCase()) {
    case 'set':
      localforage.setItem(key, value, err => err)
      break
    case 'get':
      return localforage.getItem(key, (err, value) => {
        if (err == null) {
          return value
        }
        else return null
      })
    case 'clear':
      return localforage.clear()
        .then(() => {
          console.log('清空本地数据SQL')
          return true
        })
        .catch(err => {
          console.log(err, '清空出错')
          return false
        })
    default: return null
  }
}
/**
 * @method subtraction
 * @description 减法运算
 * @subtrahend string | number 减数
 * @minuend string | number 被减数
 * */
function subtraction(subtrahend, minuend) {
  let r1, r2, m, n;
  try {
    r1 = subtrahend.toString().split(".")[1].length;
  }
  catch (e) {
    r1 = 0;
  }
  try {
    r2 = minuend.toString().split(".")[1].length;
  }
  catch (e) {
    r2 = 0;
  }
  m = Math.pow(10, Math.max(r1, r2));
  //last modify by deeka
  //动态控制精度长度
  n = (r1 >= r2) ? r1 : r2;
  return ((subtrahend * m - minuend * m) / m).toFixed(n);
}
/**
 * @method subtraction
 * @description 减法运算
 * @subtrahend string | number 减数
 * @minuend string | number 被减数
 * */
function subtraction(subtrahend, minuend) {
  let r1, r2, m, n;
  try {
    r1 = subtrahend.toString().split(".")[1].length;
  }
  catch (e) {
    r1 = 0;
  }
  try {
    r2 = minuend.toString().split(".")[1].length;
  }
  catch (e) {
    r2 = 0;
  }
  m = Math.pow(10, Math.max(r1, r2));
  //last modify by deeka
  //动态控制精度长度
  n = (r1 >= r2) ? r1 : r2;
  return ((subtrahend * m - minuend * m) / m).toFixed(n);
}
/**
 * @method agentRequest
 * @description 代理请求
 * 
 */
async function agentRequest ({api, methods, body, params}, vm) {
  let host = env.hwUrl // 重点
  let isLocal = false // 开启本地调试
  const domain = window.location.host
  if (isLocal && domain.includes('localhost')) {
    host = 'http://hwsc-dev.handeson.com/'
  }
  const config = {
    method: "post",
    body: {
      url: `${host}${api}`,
      method: methods && methods.toUpperCase() || 'GET',
      body: body || null,
      params: params || null
    }
  };
  const res = await vm.$http(agentRequestApi, config)
  if (res.httpRequestError) {
    return null
  }else {
    return res.data
  }
}
/**
 * @method function downloadExcel
 * @description get方式下载数据
 * @param {String} url 下载路径
 * @param {Object} that 只接受this
 * @param {String} extensions 扩展名
 * @return void
 */
function downloadExcel({url, that, title, extensions}) {
  let req = new XMLHttpRequest();
  title = title || '下载数据'
  req.open("get", url, true);
  req.addEventListener("loadstart", function (evt) {
    that.$message({
        message: '正在为您下载中，请耐心等待',
        type: 'info'
    })
  }, false);
    req.addEventListener("loadend", function (evt) {
      that.downloading = false
  }, false);
  req.addEventListener("ontimeout", function (evt) {  
    that.$message({
      message: '下载失败,请检查网络状态并重新尝试着刷新页面',
      type: 'error'
    })
    that.downloading = false
  }, false);
  req.addEventListener("onerror", function (evt) {
    that.$message({
      message: '下载失败,请检查网络状态并重新尝试着刷新页面',
      type: 'error'
    })
    that.downloading = false
  }, false);
  req.addEventListener("onabort", function (evt) {
    that.$message({
      message: '下载失败,请检查网络状态并重新尝试着刷新页面',
      type: 'error'
    })
    that.downloading = false
  }, false);
  req.addEventListener("onload", function (evt) {
    that.$message({
      message: '数据请求成功，请注意浏览器下载信息',
      type: 'info'
    })
    that.downloading = false
  }, false);
  req.responseType = "blob";
  req.onreadystatechange = function () {
      if (req.readyState === 4 && req.status === 200) {
        let  t = new Date();
        let  y = t.getFullYear();
        let  m = t.getMonth()+1 > 9 ? t.getMonth()+1 : `0${t.getMonth()+1}`
        let  d = t.getDate() > 9 ? t.getDate() : `0${t.getDate()}`
        let  h = t.getHours() > 9 ? t.getHours() : `0${t.getHours()}`
        let  s = t.getSeconds() > 9 ? t.getSeconds() : `0${t.getSeconds()}`
        let date = `${y}${m}${m}${d}${h}${s}`;
        let filename = title + date + (extensions || '.xls')
        if (typeof window.chrome !== 'undefined') {
            // Chrome version
            let link = document.createElement('a');
            link.href = window.URL.createObjectURL(req.response);
            link.download = filename;
            link.click();
        } else if (typeof window.navigator.msSaveBlob !== 'undefined') {
            // IE version
            let blob = new Blob([req.response], { type: 'application/force-download' });
            window.navigator.msSaveBlob(blob, filename);
        } else {
            // Firefox version
            let file = new File([req.response], filename, { type: 'application/force-download' });
            window.open(URL.createObjectURL(file));
        }
      }
      if (req.status === 0 || req.status === 500) {
        that.$message({
          message: '下载失败,请检查网络状态并重新尝试着刷新页面',
          type: 'error'
        })
        that.downloading = false
      }
  };
  req.send();
}
/**
 * @method function downExcelTemplate
 * @description 下载excel模版
 * @param {Object} data excel表格内容
 * @param {String} title excel模版名
 * @return void
 */
function downExcelTemplate (data, title) {
  const wb = { SheetNames: ['Sheet1'], Sheets: {}, Props: {} };
  const wopts = { bookType: 'xlsx', bookSST: false, type: 'binary' };//这里的数据是用来定义导出的格式类型
  wb.Sheets['Sheet1'] = XLSX.utils.json_to_sheet(data);//通过json_to_sheet转成单页(Sheet)数据
  saveAs(new Blob([s2ab(XLSX.write(wb, wopts))], { type: "application/octet-stream" }), (title || '模版') + '.' + (wopts.bookType=="biff2"?"xls":wopts.bookType));
}
function s2ab(s) {
    if (typeof ArrayBuffer !== 'undefined') {
        let buf = new ArrayBuffer(s.length);
        let view = new Uint8Array(buf);
        for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    } else {
        let buf = new Array(s.length);
        for (let i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }
}
function saveAs(obj, fileName) {
    let tmpa = document.createElement("a");
    tmpa.download = fileName || "下载";
    tmpa.href = URL.createObjectURL(obj); //绑定a标签
    tmpa.click(); //模拟点击实现下载
    setTimeout(function () { //延时释放
        URL.revokeObjectURL(obj); //用URL.revokeObjectURL()来释放这个object URL
    }, 100);
}

export {
  dateChangeover,
  transferredMeaning,
  localforageDo,
  subtraction,
  agentRequest,
  downloadExcel,
  downExcelTemplate
}