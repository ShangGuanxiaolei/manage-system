import babelpolyfill from 'babel-polyfill'
import Vue from 'vue'
import VueWorker from 'vue-worker'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import {httpRequest} from '@/service/fetch'
import VueLazyload from 'vue-lazyload'
import VueRouter from 'vue-router'
import store from '@/store'
import Vuex from 'vuex'
//import NProgress from 'nprogress'
//import 'nprogress/nprogress.css'
import routes from './routes'
// import Mock from './mock'
// Mock.bootstrap();

import axios from 'axios'
import 'font-awesome/css/font-awesome.min.css'
import '@/assets/iconfont/iconfont.css'
import '@/assets/iconfont/iconfont.js'
import md5 from 'js-md5'
import VueVideoPlayer from 'vue-video-player'
import moment from "moment"
moment.locale("zh-cn");

Vue.use(VueVideoPlayer)
Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(VueWorker)
Vue.use(VueLazyload)

//NProgress.configure({ showSpinner: false });
Vue.config.productionTip = false
Vue.prototype.$md5 = md5
Vue.prototype.$ajax = axios;
Vue.prototype.$http = httpRequest;

const router = new VueRouter({
  // mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  let user = JSON.parse(localStorage.getItem('datas'))
  let code = sessionStorage.getItem('code')
  if (to.path == '/login') {
    if(user) user.t === 0 ? localStorage.removeItem('datas') : '';
  }
  if ((!code && to.path != '/login') || (!user && to.path != '/login')) {
    next({ path: '/login' })
  } else {
    let routerArr = store.state.routerJs.routerArr
    let count = 0;
    if (to.name !== '') {
      if (routerArr.length > 5) routerArr.shift()
      routerArr.push({path:to.fullPath,name:to.name});
      for(let [index, ele] of routerArr.entries()) {
        if(to.name === ele.name) count++
        if(count > 1) routerArr.splice(index, 1)
      }
    }
    store.commit('ACTIVELIST', to.name)
    next()
    scroll(0, 0)
  }
  if(to.fullPath === '/') next({ path: 'login'})
})
//router.afterEach(transition => {
//NProgress.done();
//});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')