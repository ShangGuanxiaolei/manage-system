**demo**: [https://taylorchen709.github.io/vue-admin/](https://taylorchen709.github.io/vue-admin/)

# To start

This is a project template for [vue-cli](https://github.com/vuejs/vue-cli)

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8081
npm run dev

# build for production with minification
npm run build

```

# Folder structure
├── api
    ├── app.wpy                 //入口文件
├── common
    ├── common.vue               //配置请求地址
    ├── util.js                  //公共方法
├── components
    ├── EditBanner.vue           //新人频道banner配置公共组件
    ├── SelectTree.vue           //树形结构组件
    ├── tabs.vue                 //新人频道新人专区tab组件
    ├── verify.vue               //登录页滑块验证组件
    ├── VideoPlayer.vue          //视频播放组件
├── mock                         //假数据
├── styles                       
├── views                        //页面
    ├── bonus                    //活动管理-红包雨&抽奖
    ├── community                //社区管理-汉宝社区后台管理
    ├── hvm                      //活动管理-拼团管理、会议促销、新人频道、VIP资格设置
    ├── MicroBusiness            //微商管理-轮播图、公告&订单弹窗提示、发布管理
    ├── nav1                     //系统管理-用户、角色、权限、用户sku权限管理
    ├── onlineService            //常见问题管理-分类、问题管理
    ├── report                   //报表管理
├── home
├── login
├── main
├── vuex                          //状态管理
├── routes.js                     //路由配置

